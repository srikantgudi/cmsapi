const mongoose = require('mongoose');
const Client = require('../models/client.model.js');
const Provider = require('../models/provider.model.js')

const url = `mongodb://localhost:27017/clientsdb`;

mongoose.connect(url, { useNewUrlParser: true })
  .then(() => {
    console.log("Successfully connected to the database:", url);
  }).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
  });

exports.test = (req, res) => {
  res.send('Greetings from client test controller');
}

exports.fetch = async (req, res) => {
  let data = {clients: [], providers: []}
  // get clients
  data.clients = await Client.find();
  // get providers
  data.providers = await Provider.find()
  res.status(200).send(data);
}

exports.update = (req,res) => {
  var id = req.body._id;
  console.log('> update:', id);
  if (id) {
    Client.findOne({_id: id}, (err, doc) => {
      doc.name = req.body.name;
      doc.email = req.body.email;
      doc.phone = req.body.phone;
      doc.providers = req.body.providers;
      doc.save()
      .then(() => res.send({message: 'updated'}))
      .catch((err) => res.send({message: 'Error updating'}))
    });
  } else {
    const newClient = new Client();
    newClient.name = req.body.name;
    newClient.email = req.body.email;
    newClient.phone = req.body.phone;
    newClient.providers = req.body.providers;
    newClient.save();
  }
}

exports.delete = (req, res) => {
  Client.findByIdAndRemove(req.body._id, (err, doc) => {
    res.status(200).send({message: req.body.name + ' is deleted'})
  });
}
