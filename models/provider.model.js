const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ProviderSchema = new Schema({
  id: {type: Number, required: true},
  name: {type: String, required: true, max: 50}
});

// Export the model
module.exports = mongoose.model('Provider', ProviderSchema);
