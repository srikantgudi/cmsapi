const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ClientSchema = new Schema({
  name: {type: String, 
    required: [true, "Please enter client name"],
    max: 50},
  email: {type: String, 
    required: [true, 'Please enter valid email'],
    max: 50},
  phone:{type: String, 
    required: [true, 'Please enter a phone number'],
    max: 15},
  providers: {type: Array, required: true}
});

// Export the model
module.exports = mongoose.model('Client', ClientSchema);
