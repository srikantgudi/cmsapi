const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const clientRoute = require('./routes/client.route');
const port = 3060;
app.use(cors());
app.use('/client', clientRoute);

// routes
app.get('/', (req, res) => {
  res.send('Client Management System using Mongoose+MongoDB');
});

app.listen(port, () => {
  console.log('Running at http://localhost:'+port);
});
