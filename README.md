<style>
  .ml-2 {
    margin-left: 2em;
  }
  .mb-2 {
    margin-bottom: 2em;
  }
  .m-1 {
    margin: 1em;
  }
  .mt-1 {
    margin-top: 1em;
  }
  .mt-2 {
    margin-top: 2em;
  }
</style>
# cmsapi

### APIs for `client management system`

# Getting started

clone the api

> `git clone https://gitlab.com/srikantgudi/cmsapi.git`

# Installation

> cd cmsapi

### Install dependencies using:

> yarn

<p style="margin-left: 2em">OR</p>

> npm install

# Usage

Run the app:

> nodemon server.js

**you may need to install `nodemon` if not already done. This is used to hot reload the server on new changes.**

