const express = require('express');
const router = express.Router();

const clientController = require('../controllers/client.controller');

router.get('/test', clientController.test);

router.get('/fetch', clientController.fetch);

router.post('/update', clientController.update);

router.post('/delete', clientController.delete);

module.exports = router;
